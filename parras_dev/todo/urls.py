from django.urls import path
from .views import (
    menu_view,
    todos_list_view,
    todos_pendientes_id_titles,
    todos_pendientes_id_userid,
    todos_pendientes_sinresolver_id_title,
    todos_pendientes_resueltos_id_title,
    todos_pendientes_resueltos_id_userid,
    todos_pendientes_sinresolver_id_userid
)

urlpatterns = [
    path('', menu_view, name='menu'),  
    path('todos/pendientes_solo_ID/', todos_list_view, name='todos-html-list'),
    path('todos/pendientes_ID_title/', todos_pendientes_id_titles, name='todos-ID-title'),
    path('todos/pendientes_ID_userid/', todos_pendientes_id_userid, name='todos-ID-userid'),
    path('todos/pendientes_sinresolver_id_title/', todos_pendientes_sinresolver_id_title, name='todos-sinresolver-id-title'),
    path('todos/pendientes_resueltos_id_title/', todos_pendientes_resueltos_id_title, name='todos-resueltos-id-title'),
    path('todos/pendientes_resueltos_id_userid/', todos_pendientes_resueltos_id_userid, name='todos-resueltos-id-userID'),
    path('todos/pendientes_sinresolver_id_userid/', todos_pendientes_sinresolver_id_userid, name='todos-sinresolver-id-userID')
]

