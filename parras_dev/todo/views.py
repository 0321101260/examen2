from django.shortcuts import render
from rest_framework import generics
from .models import Todo
from .serializers import TodoSerializer

def todos_list_view(request):
    todos = Todo.objects.all().order_by('id')
    return render(request, 'todos/todos_list.html', {'todos': todos})

def todos_pendientes_id_titles(request):
    todos = Todo.objects.all().order_by('id', 'title')
    return render(request, 'todos/todos_pendientes_id_titles.html', {'todos': todos})

def todos_pendientes_id_userid(request):
    todos = Todo.objects.all().values('id', 'user__id')
    return render(request, 'todos/todos_pendientes_id_userid.html', {'todos': todos})

def todos_pendientes_sinresolver_id_title(request):
    todos = Todo.objects.filter(completed=False)  
    return render(request, 'todos/todos_pendientes_sinresolver_id_title.html', {'todos': todos})

def todos_pendientes_resueltos_id_title(request):
    todos = Todo.objects.filter(completed=True)  
    return render(request, 'todos/todos_pendientes_resueltos_id_title.html', {'todos': todos})

def todos_pendientes_resueltos_id_userid(request):
    # Filtrar solo los pendientes completados y obtener id y user_id
    todos = Todo.objects.filter(completed=True).values('id', 'user__id', 'completed') 
    return render(request, 'todos/todos_pendientes_resueltos_id_userid.html', {'todos': todos})

def todos_pendientes_sinresolver_id_userid(request):
    # Filtrar solo los pendientes completados y obtener id y user_id
    todos = Todo.objects.filter(completed=False).values('id', 'user__id', 'completed') 
    return render(request, 'todos/todos_pendientes_resueltos_id_userid.html', {'todos': todos})


def menu_view(request):
    return render(request, 'todos/menu.html')
